var $ = require('jquery'),
  swipe = require('jquery-touchswipe'),
  dragger = require('dragger'),
  slick = require('slick'),
  moment = require('moment'),
  momentRu = require('moment/locale/ru.js'),
  Pikaday = require('pikaday');

//
// start scripts
//

$(function(){

  // buttons
  if ($('.js-btn').length){ // if there are buttons on a page
    $('.js-btn').click(function(){
      $(this).toggleClass('is-active');
    });
  }

  // show and hide menu
  $('.js-nav-icon').click(function(e){
    $(this).toggleClass('is-open');
    $(this).next('.js-menu').toggleClass('is-open');
    e.stopPropagation();
  });

  $('.js-menu').click(function(e){
    e.stopPropagation();
  });

  // hide menu on click outside
  $('html').click(function(){
    if ($('.js-menu').hasClass('is-open')){
      $('.js-menu, .js-nav-icon').removeClass('is-open');
    }
  });

  // dropdown
  $('.js-dropdown-toggler').click(function(e){
    var currentBtn = $(this);
    var currentList = $(this).next('.js-dropdown-menu');

    currentList.toggleClass('is-open');
    currentBtn.toggleClass('is-active');

    $('.js-dropdown-menu').not(currentList).removeClass('is-open');
    $('.js-dropdown-toggler').not(currentBtn).removeClass('is-active');
    e.stopPropagation();
  });

  $('.js-dropdown-menu').click(function(e){
    // do not close dropdown when click on it
    e.stopPropagation();
  });

  $('html').click(function(){
    // hide dropdown on click outside
    if ($('.js-dropdown-menu').hasClass('is-open')){
      $('.js-dropdown-menu').removeClass('is-open');
      $('.js-dropdown-toggler').removeClass('is-active');
    }
  });

  // close menu on swipe
  $('.js-menu').swipe({
    swipeStatus: function(event, phase, direction){
      if (phase == 'move' && direction == 'left') {
        $('.js-menu, .js-nav-icon').removeClass('is-open');
        return false;
      }
    }
  });

  // drag menu on small desctop screen
  $('.js-menu-scroll').attachDragger();

  // accordion
  $('.js-accordion-toggler').click(function(){
    $(this).toggleClass('is-open');
    $(this).next('.js-accordion-text').slideToggle('fast');
  });

  // search slider
  $('.js-search-slider').each(function(){
    var _this = $(this);
    $(this).slick({
      dots: false,
      infinite: true,
      autoplay: false,
      slide: '.js-search-item',
      prevArrow: '<span class="slick-prev"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAIVBMVEX///+iAGeiAGeiAGeiAGeiAGeiAGeiAGeiAGeiAGeiAGfKWdTRAAAAC3RSTlMAEDC/wM/Q3+Dv8IGJCy0AAAAvSURBVHgBY0AARmYmEIXgs3Ix4eMzsaPxOcnjIwS4OEEClItwoIswshEhwgISAAClvwE4U3x9swAAAABJRU5ErkJggg=="/></span>',
      nextArrow: '<span class="slick-next"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAHlBMVEX///+iAGeiAGeiAGeiAGeiAGeiAGeiAGeiAGeiAGdm9s0eAAAACnRSTlMAEDC/wM/Q4O/wHdAjlQAAAC1JREFUeAFjAAImZkYGFMDEyUqMCBO6CAc1RJg4OJko47Oj8dkYMfgoAixIfACYRQEfvWSJ9QAAAABJRU5ErkJggg=="/></span>',
      appendArrows: $('.js-slider-nav', _this),
      appendDots: $('.js-slider-nav', _this),

      slidesToShow: 3,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 1366,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 720,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
          }
        }
      ]
    });
  });

  // image slider
  $('.js-img-slider').each(function(){
    var _this = $(this);
    $(this).slick({
      dots: false,
      infinite: true,
      autoplay: true,
      adaptiveHeight: false,
      fade: true,
      slidesToShow: 1,
      slidesToScroll: 1,

      slide: '.js-img-slide',
      prevArrow: '<span class="slick-prev"><img src="../img/icons/ar-lb.png"/></span>',
      nextArrow: '<span class="slick-next"><img src="../img/icons/ar-rb.png"/></span>',
      appendArrows: $('.js-slider-nav', _this),
      appendDots: $('.js-slider-nav', _this),

      responsive: [
        {
          breakpoint: 720,
          settings: {
            dots: false,
          }
        }
      ]
    });
  });

  // scroll navbar
  if ($('.js-menu-scroll').length){ // if there are scroll menu on a page
    var NavTop = $('.js-menu-scroll').offset().top;

    $(window).scroll(function(){

      if( $(window).scrollTop() > NavTop ){
        $('.js-menu-scroll').css({
          'position': 'fixed',
          'top': '0px',
          'padding-bottom': '10px',
          'box-shadow': '0 0 10px 0 #999',
          'z-index': '1000'
        });

        if($(window).width() < 1024) {
          $('.js-menu-scroll').css('width', '100%');
        } else {
          $('.js-menu-scroll').css('width', '980px');
        }

      } else {
        $('.js-menu-scroll').removeAttr('style');
      }
    });

    $(window).resize(function(){
      if($(window).width() < 1024) {
        $('.js-menu-scroll').css('width', '100%');
      } else {
        $('.js-menu-scroll').css('width', '980px');
      }
    });

    $('.js-menu-scroll a').click(function(e){
      var href = $(this).attr('href');
      var offsetTop = $(href).offset().top;

      // add class for current item
      var currentItem = $(this);
      currentItem.addClass('is-active');
      $('.js-menu-scroll a').not(currentItem).removeClass('is-active');

      // positioning for fixed navbar
      if ($('.js-menu-scroll').offset().top > 500){
        $('html, body').stop().animate({scrollTop: offsetTop - 60}, 2000);
      } else {
        $('html, body').stop().animate({scrollTop: offsetTop - 120}, 2000);
      }

      e.preventDefault();
    });
  }

  // modal test
  $('#js-show-modal').click(function(e){
    e.preventDefault();
    $('.modal').fadeIn();
  });

  $('.js-close-modal').click(function(){
    $('.modal').fadeOut();
  });


});
// end jQuery

// date picker
moment.locale('ru');

var picker = new Pikaday({
  field: document.getElementById('js-date'),
  format: 'D MMMM',
  firstDay: 1, // понедельник
  onSelect: function() {
    // что делать если выбран день через календарь
    // console.log(this.getMoment().format('D MMMM YYYY'));
  },
  i18n: {
    previousMonth: 'Предыдущий месяц',
    nextMonth: 'Следующий месяц',
    months: [
      'Январь',
      'Февраль',
      'Март',
      'Апрель',
      'Май',
      'Июнь',
      'Июль',
      'Август',
      'Сентябрь',
      'Октябрь',
      'Ноябрь',
      'Декабрь'
    ],
    weekdays: [
      'Воскресенье',
      'Понедельник',
      'Вторник',
      'Среда',
      'Четверг',
      'Пятница',
      'Суббота'
    ],
    weekdaysShort: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб']
  }
});

//
// end scripts
//
