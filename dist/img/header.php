<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");
$curPage = $APPLICATION->GetCurPage(true);
CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');
CModule::IncludeModule('sale');
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="windows-1251">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?$APPLICATION->ShowTitle()?></title>
	<?$APPLICATION->ShowHead();?>
  <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/style.css">
</head>
<body>
<?/*xml version="1.0" encoding="windows-1251"*/?>
<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
  <symbol id="fb" viewBox="0 0 36 36">
  	<path fill="#FFFFFF" d="M13.5,15.2h2v-1.9c0-0.8,0-2.1,0.6-2.8c0.6-0.8,1.5-1.4,3-1.4c2.4,0,3.4,0.3,3.4,0.3L22,12.3
  		c0,0-0.6-0.2-1.3-0.2c-0.7,0-1.2,0.3-1.2,1v2.2h2.6l-0.2,3h-2.4v9h-4v-9h-2V15.2z"/>
  </symbol>
  <symbol id="vk" viewBox="0 0 36 36">
    <path fill-rule="evenodd" fill="#FFFFFF" d="M12.3,11.2h6.6c2.2,0,4,1,4,3.1c0,1.8-0.6,2.4-2,3.2c0.1,0.1,0.9,0.3,1.1,0.4
  		c0.4,0.2,0.7,0.4,0.9,0.7c0.8,0.8,0.9,1.3,0.9,2.5c0,2.8-2.7,4-5.3,4h-6.3L12.3,11.2L12.3,11.2z M15.5,14.9v1.9
  		c2,0,3.4,0.2,3.4-1.6c0-1.7-1.9-1.4-3.4-1.4L15.5,14.9L15.5,14.9z M15.8,22.9c1.9,0,4.1,0.3,4.2-1.8c0-2.2-2.3-1.8-4.2-1.8
  		L15.8,22.9L15.8,22.9z"/>
  </symbol>
  <symbol id="gp" viewBox="0 0 36 36">
  	<path fill="#FFFFFF" d="M15.9,26.7c-2.2,0-3.8-1.4-3.8-3.1c0-1.6,2-3,4.2-3c0.5,0,1,0.1,1.4,0.2c1.2,0.8,2.1,1.3,2.3,2.3
  		c0,0.2,0.1,0.4,0.1,0.6C20.1,25.4,19,26.7,15.9,26.7 M16.5,16.8c-1.5,0-2.9-1.7-3.1-3.6c-0.3-1.9,0.7-3.4,2.2-3.4
  		c1.5,0,2.9,1.6,3.1,3.6C19,15.3,18,16.9,16.5,16.8 M19.6,19.7c-0.5-0.4-1.5-1.3-1.5-1.8c0-0.6,0.2-0.9,1.1-1.6
  		c1-0.7,1.6-1.7,1.6-2.9c0-1.4-0.6-3.2-1.9-3.2h1.8l1.3-1h-5.8c-2.6,0-5,1.9-5,4.1c0,2.3,1.8,4.1,4.4,4.1c0.2,0,0.4,0,0.5,0
  		c-0.2,0.3-0.3,0.7-0.3,1.1c0,0.6,0.3,1.2,0.8,1.6c-0.3,0-0.7,0-1,0c-3.2,0-5.7,2-5.7,4.1c0,2.1,2.7,3.4,5.9,3.4
  		c3.6,0,5.7-2.1,5.7-4.1C21.6,21.8,21.1,20.8,19.6,19.7 M27.5,17.2h-2v-2h-2v2h-2v2h2v2h2v-2h2V17.2z"/>
  </symbol>
  <symbol id="ok" viewBox="0 0 36 36">
  	<path fill-rule="evenodd" fill="#FFFFFF" d="M22.6,19.3c-0.3-0.6-1-1-2.1-0.2c-1.4,1.1-3.6,1.1-3.6,1.1s-2.2,0-3.6-1.1
  		c-1-0.8-1.8-0.3-2.1,0.2c-0.5,1,0.1,1.4,1.3,2.2c1,0.7,2.5,0.9,3.4,1l-0.8,0.8c-1.1,1.1-2.1,2.1-2.9,2.9c-0.4,0.4-0.4,1.1,0,1.6
  		l0.1,0.1c0.2,0.2,0.5,0.3,0.8,0.3h0c0.3,0,0.6-0.1,0.8-0.3L17,25c1.1,1.1,2.1,2.1,2.9,2.9c0.4,0.4,1.1,0.4,1.6,0l0.1-0.1
  		c0.4-0.4,0.4-1.1,0-1.6l-2.9-2.9L18,22.5c0.9-0.1,2.4-0.3,3.4-1C22.6,20.7,23.1,20.2,22.6,19.3L22.6,19.3z M17,11.4
  		c1.3,0,2.4,1.1,2.4,2.4c0,1.3-1.1,2.4-2.4,2.4s-2.4-1.1-2.4-2.4C14.6,12.4,15.7,11.4,17,11.4L17,11.4z M17,18.7
  		c2.7,0,4.9-2.2,4.9-4.9c0-2.7-2.2-4.9-4.9-4.9c-2.7,0-4.9,2.2-4.9,4.9C12.1,16.5,14.3,18.7,17,18.7L17,18.7z"/>
  </symbol>
</svg>
<!-- sidebar -->
<aside class="c-sidebar">
  <img class="c-logo--mobile" src="<?=SITE_TEMPLATE_PATH?>/img/logo-main.png" alt="����� �������� ��������������">
  <div class="c-nav-icon js-nav-icon">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
  </div>
  <div class="c-sidebar__content js-menu">
    <div class="c-logo">
      <a href="<?=SITE_DIR?>">
        <img class="c-logo--big" src="<?=SITE_TEMPLATE_PATH?>/img/logo-icon.svg" alt="������� ����� �������� ��������������">
        <img class="c-logo--main" src="<?=SITE_TEMPLATE_PATH?>/img/logo-main.svg" alt="����� �������� ��������������">
      </a>
    </div>
    <form action="<?=SITE_DIR."search/"?>" class="c-search">
      <input type="search" name='q' class="c-search__input" placeholder="����� �������">
    </form>
    <nav>
    <?$APPLICATION->IncludeComponent("bitrix:menu", "top", array(
							"ROOT_MENU_TYPE" => "top",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_TIME" => "36000000",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_THEME" => "site",
							"CACHE_SELECTED_ITEMS" => "N",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MAX_LEVEL" => "1",
							"CHILD_MENU_TYPE" => "left",
							"USE_EXT" => "N",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N",
						),
						false
					);?>
    </nav>
    <div class="c-info">
      <div class="c-info__widget">
        <a href="http://museum.skipodevelop.ru/visit/#1"><time><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/time.php"), false);?></time></a>
      </div>
      <div class="c-info__widget">
        <a href="http://museum.skipodevelop.ru/visit/#3"><address><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/address.php"), false);?></address></a>
      </div>
      <div class="c-info__widget">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.site.selector",
	"museum",
	Array(
		"COMPONENT_TEMPLATE" => "museum",
		"SITE_LIST" => array("*all*"),
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600"
	)
);?>
      </div>
    </div>
  </div>
</aside>
<!-- / sidebar -->
  <main>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<?
$isDetail = ($_REQUEST["ELEMENT_CODE"] && CSite::InDir(SITE_DIR.'tickets/'))? true: false;
if(CSite::InDir(SITE_DIR.'index.php'))
{
?>
    <div class="container">
<?
}
else if(CSite::InDir(SITE_DIR.'visit/'))
{
	?>
    <div class="container container--wide container--sh">
      <section class="o-block o-block--banner t-block--lead t-block--visit">
        <div class="o-block__footer">
          <h2 class="c-heading c-heading--huge t-heading--contrast"><?$APPLICATION->ShowTitle(false);?></h2>
        </div>
      </section>
      <nav class="u-menu-container">
      <?$APPLICATION->IncludeComponent("bitrix:menu", "page", array(
							"ROOT_MENU_TYPE" => "visit",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_TIME" => "36000000",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_THEME" => "site",
							"CACHE_SELECTED_ITEMS" => "N",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MAX_LEVEL" => "1",
							"CHILD_MENU_TYPE" => "left",
							"USE_EXT" => "N",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N",
						),
						false
					);?>
      </nav>
      <!-- double text -->
	<?
}
else if($isDetail)
{
	?>
	<?
}
else if(CSite::InDir(SITE_DIR.'museum/'))
{
	?>
    <div class="container container--wide container--sh">
      <section class="o-block o-block--banner t-block--lead t-block--museum">
        <div class="o-block__footer">
          <h2 class="c-heading c-heading--huge t-heading--contrast"><?$APPLICATION->ShowTitle(false);?></h2>
        </div>
      </section>
      <nav class="u-menu-container">
      <?$APPLICATION->IncludeComponent("bitrix:menu", "page", array(
							"ROOT_MENU_TYPE" => "museum",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_TIME" => "36000000",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_THEME" => "site",
							"CACHE_SELECTED_ITEMS" => "N",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MAX_LEVEL" => "1",
							"CHILD_MENU_TYPE" => "left",
							"USE_EXT" => "N",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N",
						),
						false
					);?>
      </nav>
      <!-- double text -->
	<?
}
else if(CSite::InDir(SITE_DIR.'search/'))
{
	?>
    <div class="container">
      <h2 class="c-heading c-heading--huge"><?$APPLICATION->ShowTitle(false);?></h2>
      <form action="<?=SITE_DIR."search/"?>" class="c-search">
        <input type="search" name='q' class="c-search__input" value='<?=($_REQUEST["q"])? htmlspecialchars($_REQUEST["q"]): "";?>' placeholder="����� �������">
      </form>
	<?
}
else if(CSite::InDir(SITE_DIR.'calendar/') || CSite::InDir(SITE_DIR.'tickets/'))
{
	$menu = (CSite::InDir(SITE_DIR.'calendar/'))? 'calendar': 'tickets';
	?>
    <div class="container container--wide">
      <!-- ��������� �������� -->
      <section class="o-block o-block--banner t-block--lead t-block--visit">
        <div class="o-block__footer">
          <h2 class="c-heading c-heading--huge t-heading--contrast"><?$APPLICATION->ShowTitle(false);?></h2>
        </div>
      </section>
      <nav class="u-menu-container">
      <?$APPLICATION->IncludeComponent("bitrix:menu", "page", array(
							"ROOT_MENU_TYPE" => $menu,
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_TIME" => "36000000",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_THEME" => "site",
							"CACHE_SELECTED_ITEMS" => "N",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MAX_LEVEL" => "1",
							"CHILD_MENU_TYPE" => "left",
							"USE_EXT" => "N",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N",
						),
						false
					);?>
      </nav>
      <!-- / ��������� �������� -->
      <!-- container -->
      <div class="container">
      <!-- double text -->
	<?
}
else
{
	?>
    <div class="container container--wide">
      <section class="o-text">
	<?
}?>



